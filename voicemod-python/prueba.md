Había 3 ejercicios, cada uno de ellos tenía 3 pruebas a pasar: que funcionara para el caso de ejemplo, que funcionara para todos los casos y que funcionara con una cantidad de datos muy grande en tiempo relativamente bajo (Probablemente un timeout de unos 20-30 segundos aprox.).

##
El primer ejercicio consistía en devolver la suma de los dos números mayores de uns lista.

Ejemplo:
    lista = [53, 27, 35, 18]
    resultado = 88

##
En el segundo ejercicio había que implementar una clase Veterinario que tuviera dos métodos, el de añadir animal a la lista de espera y el de curar el animal que estuviera primero en la lista de espera.

Ejemplo:
    lista de espera = [Bobby, Tobby, Dobby]
    añadir(Kitty) ----> [Bobby, Tobby, Dobby, Kitty]
    curar() ----> [Tobby, Dobby, Kitty]

##
El último consistía en comprobar si la suma de 3 números consecutivos de una lista era igual a un número dado.

Ejemplo 1:
    número = 54
    lista = [23, 14, 40, 9]

    resultado -----> True


Ejemplo 2:
    número = 51
    lista = [23, 14, 40, 9]

    resultado -----> False
