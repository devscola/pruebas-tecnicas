# Lista de pruebas tecnicas

## Añadir nueva prueba tecnica

1. Crea una carpeta con [nombreempresa]-[lenguaje]. En caso de que la prueba tecnica se pueda hacer en multiples lenguajes, utiliza [nombreempresa]-any
2. Dentro de la carpeta, añade un archivo `prueba.md` con la descripcion de la tarea.
3. Si tienes las preguntas que hacen en la revision de la tarea, añade un archivo `revision.md` con las preguntas
4. Si la prueba venia con codigo, añadelo dentro de la carpeta.
