# Prueba técnica ReactJS - Finametrix.

### Descripción:  

**Se trata de montar una web con:**

Una página inicial que contenga un top 3 de las criptomonedas *(lista más abajo)* con
más market cap (capitalización de mercado), buscamos una web que nos sea útil para ver toda la información relacionada a estas criptomonedas, por lo tanto:

  - a. Debemos poder crear un buscador que nos vaya filtrando por nombre las
criptomonedas que tengamos en la vista.

  - b. Debemos poder ordenar (ascendente y descendente) por:
    - i. Market Cap
    - ii. Precio de cierre
    - iii. Volumen


  - c. Si le damos clic a una de ellas, debemos poder ver más información
relacionada con esta:
   - i. El nombre de la criptomoneda y en la moneda en la que se está
representando.
    - ii. La fecha de la última actualización.
    - iii. Una gráfica con todos los precios de cierre desde inicio hasta a día de
hoy.
    - iv. Un filtro que nos permita desde la misma pantalla recargar los datos
de la tabla con otra divisa.


  - d. La web debe ser responsive en la medida de lo posible.

### Valorado positivamente:
1. Uso de un un state management para React
2. Usa todas las buenas prácticas que conozcas.
3. Usa el motor de estilos que más te guste, tanto con frameworks o sin.
4. Implementar una nueva funcionalidad a tu gusto.

### API a consultar para obtener las criptomonedas:
- https://www.alphavantage.co/documentation/

- La API tiene una pequeña limitación de 5 llamadas simultaneas por minuto, por lo tanto, una manera de solucionarlo en un entorno de desarrollo es hacer una única llamada guardarse el JSON (respuesta) y programar sobre esto, eso sí, cuando nos los pases deben estar apuntando a los enlaces de producción, o simplemente programar con esta limitación.
- Obtener los precios de cada día desde el inicio:
  - https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY

### Top 10 Criptomonedas:
1. BTC, Bitcoin
2. ETH, Ethereum
3. XRP, Ripple
