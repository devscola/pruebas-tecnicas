# Preguntas en la entrevista técnica

- Explica el desarrollo que has realizado: arquitectura, estructura...
- Imagina que tienes que meter un sistema de caché para poder utilizar el wikireader de manera offline. ¿Cómo lo harías? ¿Qué ocurre con el comando de `RANDOM`?
- En lugar de utilizar un REPL, queremos integrar el wikireader con `Amazon Alexa`. ¿Qué cambios tendrías que realizar?
