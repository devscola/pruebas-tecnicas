# Task: Wiki Reader

Write a Javascript (nodejs) application that allows reading Wikipedia from the command line.

The application must accept a language code parameter that defines which language articles will be read from. For example, specifying “en” will allow reading “en.wikipedia.org” and “fr” will use “fr.wikipedia.org. The application should validate that the language code correspond to an
existing version of the encyclopedia.

Once opened, the application is a REPL (​ https://en.wikipedia.org/wiki/Read-eval-print_loop​ ) that accepts the following commands:

- READ <article>: Print the article text. It must be plain text, not wikitext markup or HTML. It must print the text on paragraph at a time, waiting for the user to press a key before printing the next paragraph. The user must be able to stop reading an article with a special key.
- QUIT: Exit the application.
- HELP: Print the available commands with a short description.
- ABOUT <article>: Show metadata about an article
	- Creation date
	- Username of the author of the first revision
	- Last modified date
	- Username of the author or the last revision
	- Size in bytes
	- Any other info that you think is relevant
- RANDOM: Behave like the READ command above but with a random article.

In the commands above, <article> refers to the article title as seen in the URL. Spaces are converted to underscores and special characters are encoded. For example, the English Wikipedia page for Arundhati Roy is at ​ https://en.wikipedia.org/wiki/Arundhati_Roy

Your solution must include a “readme” file that explains how to run it.

Automated unit or integration tests are a nice-to-have.

Do NOT spend more than 3 hours on this task. The “about” and “random” features are optional. Only work on them if you have time.
