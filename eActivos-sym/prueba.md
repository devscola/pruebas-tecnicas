eActivos
Prueba técnica
El objetivo de esta prueba técnica consiste en realizar un pequeño proyecto que consistirá en
desarrollar con Symfony 3 o 4 un pequeño proyecto para evaluar los conocimientos del candidato.
Este proyecto consiste en desarrollar una pequeña plataforma de gestión de subastas en la que
hay varios actores. Estos actores son:
-
-
Usuario (Los que realizan pujas sobre las subastas)
Administrador (Para poder gestionar las subastas)
Los Usuarios podrán:
- Registrarse
- Iniciar sesión / login
- Ver un listado de llas subastas
- Pujar en subastas.
- Cancelar una puja suya
Los administradores podran:
- crear subastas
- editar subastas
- eliminar subastas
- listar subastas
Para ello, deberás implementar una aplicación web donde se vean las subastas activas (puedes
hacer una precarga de datos para que el listado no este vacio inicialmente).
Obligatorio:
- Desarrollar la aplicación con los puntos de arriba mínimos.
- Usar Docker para poder correr el proyecto en el entorno local.
Opcional (Sumará más puntos)
- Limpieza, orden, organización, naming, buenas prácticas y coherencia del código.
- Arquitectura Hexagonal / DDD
- Tests unitarios y/o funcionales
- Responsive (cualquier Framework)
- Uso de algún framework JS (VueJs, React) para alguna parte de la aplicacion.
- Redacción de un documento Readme para arrancar la aplicación, lanzar tests, migraciones
de la Base de Datos, Datafixtures, etc.
- PSR-2
- Git / Git-flow
Indicaciones:
No es necesario un gran desarrollo ni mucha complejidad, solo es necesario cumplir los requisitos
nombrados anteriormente para poder evaluar los conocimientos del candidato.